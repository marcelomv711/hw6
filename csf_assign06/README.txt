 In a file called README.txt, briefly describe how you made the calculator instance’s shared data safe to access from multiple threads. 
 Indicate what kind of synchronization object you used, and how you determined which regions of code were critical sections.

Whenc onsturction the multithreaded calculator, we used a mutex to o protect the shared calculator data strcuture. A mutex was used to lock the variables used
by the calculator and the mutex was unlocked once the calculations of the calculator were performed. The critical section identified was during the calculations when the variable data structure could be altered. 
This mutex allowed us to provide a convenien way to ensure mutually exclusive access 
to shared variables.

Furthermore, the semaphore utlized limited the number of threads used. This was done by having a NUM_THREADS variable that indicated the maxmimum number of threads,
 which was  5. In addition, the nature of semaphores, their two special operations of P and V, ensures that a compiling program can never enter 
a state where an instantiated semaphore has a negative value. This specific quality of semaphores allows us to control the trajectories of concurrent 
programs and ensure that our multithreaded calculator works as expected.



