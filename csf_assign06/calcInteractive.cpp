/*
 * Interactive calculator program
 *
 * This should work correctly once you have implemented
 * and tested your calc_ functions
 */

#include <stdio.h>      /* for snprintf */
#include "csapp.h"      /* for rio_ functions */
#include "calc.h"
#include <semaphore.h>

/* buffer size for reading lines of input from user */
#define LINEBUF_SIZE 1024
#define NUM_THREADS 4 //should change?

typedef struct {
	Calc* calc;
	pthread_mutex_t lock;
} Shared;


/*
 * Data structure representing a client connection.
 */
struct ConnInfo {
	int clientfd;
	const char *webroot;
};

void *worker(void *arg) {
	struct ConnInfo *info = arg;

	/*
	 * set thread as detached, so its resources are automatically
	 * reclaimed when it finishes
	 */
	pthread_detach(pthread_self());

	/* handle client request */
	server_chat_with_client(info->clientfd, info->webroot);
	close(info->clientfd);
	free(info);

	return NULL;
}


//chat_with_client(info->calc, info->client_fd)
//server_chat_with_client(info->clientfd, info->webroot);




void chat_with_client(struct Calc *calc, int infd, int outfd);

int main(void) {
	struct Calc *calc = calc_create();

	//added
	Shared* obj = callock(1, sizeof (Shared));
	pthread_mutex_init(&obj->lock, NULL);
	pthread_t threads[NUM_THREADS];
	for (int i = 0; i < NUM_THREADS; i++)
		pthread_create(&threads[i], NULL, worker, obj); //worker is the action that needs to be done
	for (int i = 0; i < NUM_THREADS; i++)
		pthread_join(threads[i], NULL);

	pthread_mutex_lock(&obj->lock);
	pthread_mutex_unlock(&obj->lock);//at end should unlock it



	/* chat with client using standard input and standard output */
	chat_with_client(calc, 0, 1);

	calc_destroy(calc);

	return 0;
}

void chat_with_client(struct Calc *calc, int infd, int outfd) {
	rio_t in;
	char linebuf[LINEBUF_SIZE];

	/* wrap standard input (which is file descriptor 0) */
	rio_readinitb(&in, infd);

	/*
	 * Read lines of input, evaluate them as calculator expressions,
	 * and (if evaluation was successful) print the result of each
	 * expression.  Quit when "quit" command is received.
	 */
	int done = 0;
	while (!done) {
		ssize_t n = rio_readlineb(&in, linebuf, LINEBUF_SIZE);
		if (n <= 0) {
			/* error or end of input */
			done = 1;
		} else if (strcmp(linebuf, "quit\n") == 0 || strcmp(linebuf, "quit\r\n") == 0) {
			/* quit command */
			done = 1;
		} else {
			/* process input line */
			int result;
			if (calc_eval(calc, linebuf, &result) == 0) {
				/* expression couldn't be evaluated */
				char out[] = "Error\n";
				rio_writen(outfd, out, 6);
			} else {
				/* output result */
				int len = snprintf(linebuf, LINEBUF_SIZE, "%d\n", result);
				if (len < LINEBUF_SIZE) {
					rio_writen(outfd, linebuf, len);
				}
			}
		}
	}
}
