//Marcelo Morales lmoral10@jhu.edu
//Jeremy Okeyo jokeyo2@jhu.edu

#include <vector>
#include <stdio.h>
#include <ctype.h>
#include <stdexcept>
#include <string> 
#include <iostream> 
#include <map>
#include "calc.h"
#include <mutex>

using std::vector;
using std::string;
using std::map;
using std::cout;
using std::endl;
using std::stoi;

struct Calc {
};

//CalcImpl class
class CalcImpl: public Calc {
	public:
		int evalExpr(const char *expr, int *results);
		map<string, int> variables;
		std::mutex variables_mutex;
	private:
	int checkFirstElem(char var);
	void chat_with_client(struct Calc *calc, int infd, int outfd);
	int assignment(vector<string> expression, int currentIndex);
	int operation(vector<string> expression, int currentIndex);
//	bool checkOperator(char var);
	int do_math(string operand, int number_1, int number_2);
	vector<string> tokenize(const string &expr);
	bool check_alphanumeric(vector<string> tokens);
	bool find_variable(string token);
};
extern "C" struct Calc *calc_create(void) {
    return new CalcImpl();
}

extern "C" void calc_destroy(struct Calc *calc) {
    CalcImpl *obj = static_cast<CalcImpl *>(calc);
    delete obj;
}

extern "C" int calc_eval(struct Calc *calc, const char *expr, int *result) {
    CalcImpl *obj = static_cast<CalcImpl *>(calc);
    return obj->evalExpr(expr, result);
}

//Evaluates the expression
int CalcImpl::evalExpr(const char *expr, int *results) {
	string expression(expr);
	vector<string> tokens = tokenize(expression);
	if (!check_alphanumeric(tokens)) {
		return 0; //FAIL :(
	}

	try {
		variables_mutex.lock();
		if (checkFirstElem((tokens.at(0)).at(0)) == 0) {
			return 0; //FAIL :(
		}
		else if (checkFirstElem((tokens.at(0)).at(0)) == 1) {
			if (tokens.size() > 2 && tokens.at(1).at(0) == '=') {
				*results = assignment(tokens, 0);
			} else {
				*results = operation(tokens, 0);
			}
		} else {
			*results = operation(tokens, 0); // just a number
		}
		variables_mutex.unlock();
	}
	catch (std::invalid_argument &e) {
			variables_mutex.unlock();
   		cout << "Error" << endl;
		   return 0;
	}
	return 1; //EVERYHTING PASSED :)
}


//Performs expression that involve assignments
int CalcImpl::assignment(vector<string> expression, int currentIndex) {
	if (currentIndex + 2 > expression.size()) {
		throw std::invalid_argument("Error: Invalid assignment");
	}

	int updated_value = operation(expression, currentIndex+2);
	string variable = expression.at(currentIndex);
	variables[variable] = updated_value; //update our map!

	return updated_value;
}

//Peforms expressions that do not involve assignments
int CalcImpl::operation(vector<string> expression, int currentIndex) {
	string variable = expression.at(currentIndex);
	int value_1 = -1;
	if (checkFirstElem(variable[0]) == 1) {
		if (find_variable(variable)) { //variable is here, 
			value_1 = variables[variable];
		} else {
			throw std::invalid_argument("Error: undefined variable");
		}
	} 
	else {//1 + 1
		value_1 = stoi(variable);
	}

	int result = value_1;
	if (currentIndex + 2  < expression.size()) {
		variable = expression.at(currentIndex + 2);
		int value_2 = -1;
		
		if (checkFirstElem(variable[0]) == 1) {
			if (find_variable(variable)) { //variable is here, 
				value_2 = variables[variable];
			} else {
				throw std::invalid_argument("Error: undefined variable");
			}
		} 
		else {//1 + 1
			value_2 = stoi(variable);
		}
	
		string operand = expression.at(currentIndex+1).c_str();
    	result = do_math(operand, value_1, value_2);
	} else if (currentIndex + 1 < expression.size()) {
		throw std::invalid_argument("Error: invalid operands");
	}
	return result;
}

//this throws exception so catch exception when method called
int CalcImpl::do_math(string operand, int number_1, int number_2) {
	char op = operand[0];
	switch (op){
		case '+':
			return number_1 + number_2;
		case '-':
			return number_1 - number_2;
		case '*':
			return number_1 * number_2;
		case '/':
			if (number_2 == 0) {
				throw std::invalid_argument( "Error: dividing by zero" );
			}
			return number_1 / number_2;
		default: //should never get
			throw std::invalid_argument( "Error: operand not valid" );
	}

}



//returns true if the variable is in the map
bool CalcImpl::find_variable(string token) {
	return CalcImpl::variables.find(token) != variables.end();  //returns true if iterator rwched the end, that meand nothing was foind
}

//checks what the fist Element is
// 0 - Invalid , 1 - variable, 2 - operand
//checks if doing an assignment or a simple operation
int CalcImpl::checkFirstElem(char var) {
	if (var >= 'A' && var <= 'Z') {
		return 1;
	} else if (var >= 'a' && var <= 'z') {
		return 1;
	} else if ((var >= '0' && var <= '9') || var == '-') {
		return 2;
	} else {
		return 0;
	}
}

//Tokenizes expression by removing spaves and separating operands and operators
vector<string> CalcImpl::tokenize(const string &expr) {
	// Vector of string to save tokens 
    vector <string> tokens; 
    string intermediate; 
	 unsigned len = expr.length();
	for(unsigned i = 0; i < len; i++) {
		if (expr[i] == ' ') {
			tokens.push_back(intermediate);
			intermediate = "";
			i++;
		}
		if (i == len - 1) {
			if (expr[i] != '\n') {
				intermediate.push_back(expr[i]);
			}
			tokens.push_back(intermediate);
		}
		intermediate.push_back(expr[i]);
	}
	return tokens;
}


//checks all integers are all digits, checks all strings are alphabetic
bool CalcImpl::check_alphanumeric(vector<string> tokens) {
	string current = "";
	for (unsigned i = 0; i < tokens.size(); i++) {
		current = tokens.at(i);
		char first_char = current[0];
		int all_letters = isalpha(first_char); 
		for (unsigned j = 1; j < current.length(); j++) {
				if (isalpha(current[j]) != all_letters) {
					return false;
				} 
			}  
		}
	return true;
}
