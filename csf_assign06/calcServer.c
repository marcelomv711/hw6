//Marcelo Morales lmoral10@jhu.edu
//Jeremy Okeyo jokeyo2@jhu.edu


#include <stdio.h>      
#include <stdlib.h>
#include "csapp.h"
#include "calc.h"
#include <semaphore.h>
#include <pthread.h>

/* buffer size for reading lines of input from user */
#define SIZE_OF_BUFFER 1024
#define NUM_THREADS 5
volatile int shut_down = 0;

/*
Main function for connecting to the calculator server
*/


//Data structure representing the number of active threads
struct threads{
	int num_threads;
};

/*
 * Data structure representing a client connection.
 */
struct ConnInfo {
	struct Calc *calc;
	int clientfd;
	struct threads *threads;
};

void *worker(void *arg) {
	struct ConnInfo *info = arg;

	/*
	 * set thread as detached, so its resources are automatically
	 * reclaimed when it finishes
	 */
	pthread_detach(pthread_self());
	int exit = -1;
	int loopCounter = 0;
	while (exit == -1) {
			exit = chat_with_client(info->calc, info->clientfd, info->clientfd);
			close(info->clientfd);
	}
	//Shutdown command issued
	if (exit == 0) {
		shut_down = 1;
	}
	/* handle client request */
	sem_post(&info->threads->num_threads);
	free(info);
	return NULL;
}

int main(int argc, char **argv) {
	//Initializes sempahore 
	struct threads *threads = malloc(sizeof(struct threads));
	sem_init(&threads->num_threads, 0, NUM_THREADS);
	if (argc != 2) {
       return 0;
   }
	struct Calc *calc = calc_create();
	int port = atoi(argv[1]);
	 if (port < 1024) {
		  printf("A free port is not available \n");
		  return 0;
	 }
	 int listen = Open_listenfd(argv[1]);
	 if (listen < 0) {
		  printf("Server is not opened! \n");
		  return 0;
	 }

	 int exit = 1;
	 int loopCounter = 0;
	 while (exit) {
	 	//Checks whether shutdown command was issued
	 	if (!shut_down) {
			sem_wait(&threads->num_threads);//Waits for available thread slots
			int clientfd = Accept(listen, NULL, NULL);
			if (clientfd > 0) {//Connection established
				struct ConnInfo *info = malloc(sizeof(struct ConnInfo));
				info->calc = calc;
				info->clientfd = clientfd;
				info->threads = threads;
				pthread_t thr_id;
				Pthread_create(&thr_id, NULL, worker, info); 
			}
			//SHutdown with no threads running
		} else if (shutdown && threads->num_threads == NUM_THREADS) {
			exit = 1;
		}
	 }
	 sem_destroy(&threads->num_threads);
	 close(listen);
	 calc_destroy(calc);
	return 0;
}

/*
Connecting with the client server
*/

int chat_with_client(struct Calc *calc, int infd, int outfd) {
	rio_t in;
	char linebuf[SIZE_OF_BUFFER];

	rio_readinitb(&in, infd);

    printf("check");
    while (1) {
		ssize_t n = rio_readlineb(&in, linebuf, SIZE_OF_BUFFER);
		if (n <= 0) {
			/* error or end of input */
		} else if (strcmp(linebuf, "quit\n") == 0 || strcmp(linebuf, "quit\r\n") == 0) {
			/* quit command */
			return 1;
		} else if (strcmp(linebuf, "shutdown\n") == 0 || strcmp(linebuf, "shutdown\r\n") == 0){
            return 0;
        } else {
			/* process input line */
			int result;
			if (calc_eval(calc, linebuf, &result) == 0) {
				/* expression couldn't be evaluated */
				rio_writen(outfd, "Error\n", 6);
			} else {
				/* output result -> result works */
				int len = snprintf(linebuf, SIZE_OF_BUFFER, "%d\n", result);
				if (len < SIZE_OF_BUFFER) {
					rio_writen(outfd, linebuf, len);
				}
			}
		}
	}
    return 1;
}
